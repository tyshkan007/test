package me.show.testscroll;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.RelativeLayout;

import me.show.testscroll.gallery.Scroll;


/**
 * Created by Anton Alexandrov on 02.09.15.
 * All rights reserved.
 */
public class Main extends Activity {

    public static final String SCROLL_FRAGMENT = "scrollFragment";
    Scroll scroll;
   // mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.main_activity);
      //  RelativeLayout topLayout = (RelativeLayout) findViewById(R.id.topLayout);

        scroll = (Scroll) getFragmentManager().findFragmentByTag(SCROLL_FRAGMENT);
        if (scroll == null) {
            scroll = new Scroll();
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.topLayout, scroll, SCROLL_FRAGMENT);
        ft.commit();
    }


}