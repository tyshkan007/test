package me.show.testscroll.contentUtil;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import me.show.testscroll.dbPack.Contract;
import me.show.testscroll.imageUtil.ImageUtil;

public class SaveContentService extends IntentService {

    public static final String ADD_GENRE = "addGenre";
    public static final String ADD_MOVIE = "addMovie";


    public SaveContentService() {
        super("saveService");
    }

    public void onCreate() {
        super.onCreate();

    }

    private ImageUtil imageUtil = new ImageUtil();

    @Override
    protected void onHandleIntent(Intent intent) {
        if(intent.getAction().equals(ADD_GENRE)) {
            String patch = saveImage(intent);

            ContentValues contentValues = new ContentValues();
            contentValues.put(Contract.Genre.NAME, intent.getStringExtra("name"));
            contentValues.put(Contract.Genre.PREVIEW, patch);

            getContentResolver().insert(Contract.Genre.CONTENT_URI, contentValues);

        } else if(intent.getAction().equals(ADD_MOVIE)) {

            String patch = saveImage(intent);

            ContentValues contentValues = new ContentValues();
            contentValues.put(Contract.Movie.NAME, intent.getStringExtra("name"));
            contentValues.put(Contract.Movie.PREVIEW, patch);

            Uri insert = getContentResolver().insert(Contract.Movie.CONTENT_URI, contentValues);

            String[] genres = intent.getStringArrayExtra("genres");
            for(String genre : genres){
                Cursor cursor = getContentResolver().query(
                    Contract.Genre.CONTENT_URI,
                    new String[] {Contract.Genre._ID},
                    "name=?",
                    new String[]{genre},
                    null
                );
                if (cursor != null && cursor.moveToFirst()) {
                    int id = cursor.getInt(cursor.getColumnIndex(Contract.Genre._ID));

                    contentValues = new ContentValues();
                    contentValues.put(Contract.GenreAndMovie.ID_GENRE, id);
                    contentValues.put(Contract.GenreAndMovie.ID_MOVIE, insert.getLastPathSegment());

                    getContentResolver().insert(Contract.GenreAndMovie.CONTENT_URI, contentValues);
                    cursor.close();
                } else {
                    throw new RuntimeException("bad request");
                }
            }
        }
    }

    private String saveImage(Intent intent) {
        String name = intent.getStringExtra("name");
        int imageIdRes = intent.getIntExtra("imageIdRes", -1);
        Bitmap preview = BitmapFactory.decodeResource(getResources(), imageIdRes);
        return  imageUtil.saveImage(getApplicationContext(), preview, name);
    }

}
