package me.show.testscroll.contentUtil;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Anton Alexandrov on 03.09.15.
 * All rights reserved.
 */
public class ControllerContent {


    public void addGenre(Context context, int imageIdRes, String name) {
        Intent intent = new Intent(context, SaveContentService.class);
        intent.setAction(SaveContentService.ADD_GENRE);
        intent.putExtra("imageIdRes", imageIdRes);
        intent.putExtra("name", name);
        context.startService(intent);
    }


    public void addMovie(Context context, int idRes, String name, String[] genreNames){
        Intent intent = new Intent(context, SaveContentService.class);
        intent.setAction(SaveContentService.ADD_MOVIE);
        intent.putExtra("imageIdRes", idRes);
        intent.putExtra("name", name);
        intent.putExtra("genres", genreNames);
        context.startService(intent);
    }

}
