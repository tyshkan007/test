package me.show.testscroll.imageUtil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.annotation.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by Anton Alexandrov on 03.09.15.
 * All rights reserved.
 */
public class ImageUtil {

    @Nullable
    public String saveImage(Context context, Bitmap b, String name){
        try {
            String path = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString();
            File wallpaperDirectory = new File(path + "/img");
            wallpaperDirectory.mkdirs();
            long time = System.currentTimeMillis();
            String nameFile = name + String.valueOf(time) + ".jpg";
            File outputFile = new File(wallpaperDirectory, nameFile);
            FileOutputStream outStream = new FileOutputStream(outputFile);
            b.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            return nameFile;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    //TODO вынести из основного потока
    @Nullable
    public Bitmap getImageBitmap(Context context, String name){
        try {
            String path = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString();
            File imgFile = new File(path + "/img/" + name);
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            return bitmap;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
