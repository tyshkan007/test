package me.show.testscroll.dbPack;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created by Anton Alexandrov on 02.09.15.
 * All rights reserved.
 */
public class MovieProvider extends ContentProvider {


    private final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private DBHelper dbHelper;

    public static final int GENRE = 0;
    public static final int GENRE_MOVIE = 1;
    public static final int MOVIE = 2;
    public static final int MOVIE_BY_ID_GENRE = 3;

    @Override
    public boolean onCreate() {

        dbHelper = new DBHelper(getContext());
        uriMatcher.addURI(Contract.AUTHORITY, "genre", GENRE);
        uriMatcher.addURI(Contract.AUTHORITY, "genre/movie", GENRE_MOVIE);
        uriMatcher.addURI(Contract.AUTHORITY, "genre/movie/*", MOVIE_BY_ID_GENRE);
        uriMatcher.addURI(Contract.AUTHORITY, "movie", MOVIE);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor;
        switch (uriMatcher.match(uri)) {
            case GENRE:
                cursor = dbHelper.getReadableDatabase().query(
                    Contract.Genre.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder
                );
                cursor.setNotificationUri(getContext().getContentResolver(), Contract.Genre.CONTENT_URI);
                return cursor;
            case MOVIE_BY_ID_GENRE:
                String sqlQuery = "SELECT f.* FROM genreMovie gF INNER JOIN movie f ON gf.idMovie=f._id WHERE gF.idGenre < ?";
                cursor = dbHelper.getReadableDatabase().rawQuery(sqlQuery, new String[]{uri.getLastPathSegment()});
                cursor.setNotificationUri(getContext().getContentResolver(), Contract.Genre.CONTENT_URI);
                return cursor;
        }
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        switch (uriMatcher.match(uri)) {
            case GENRE:
                return insert(Contract.Genre.TABLE_NAME, values, Contract.Genre.CONTENT_URI);
            case GENRE_MOVIE:
                return insert(Contract.GenreAndMovie.TABLE_NAME, values, Contract.GenreAndMovie.CONTENT_URI);
            case MOVIE:
                return insert(Contract.Movie.TABLE_NAME, values, Contract.Movie.CONTENT_URI);
            default:
                throw new RuntimeException();
        }
    }

    private Uri insert(String tableName, ContentValues values, Uri uri) {
        long rowID = dbHelper.getWritableDatabase().insert(tableName, null, values);
        if(rowID == -1){
            throw new RuntimeException("bad insert");
        }
        Uri resultUri = ContentUris.withAppendedId(uri, rowID);
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }



    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }


}
