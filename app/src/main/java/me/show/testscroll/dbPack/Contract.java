package me.show.testscroll.dbPack;

import android.net.Uri;

/**
 * Created by Anton Alexandrov on 06.08.15.
 */
public class Contract {

    public static final String AUTHORITY = "me.show.testscroll.dbPack";
    public static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);

    private Contract() {}

    public static final class Genre {

        private Genre() {}

        public static final String TABLE_NAME = "genre";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "genre");

        public static final String _ID = "_id";
        public static final String NAME = "name";
        public static final String PREVIEW = "preview";

    }

    public static final class Movie {

        private Movie() {}

        public static final String TABLE_NAME = "movie";

        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "movie");

        public static final String _ID = "_id";
        public static final String NAME = "name";
        public static final String PREVIEW = "preview";

    }

    public  static  final class GenreAndMovie {

        private GenreAndMovie(){}

        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "genre/movie");

        public static final String TABLE_NAME = "genreMovie";
        public static final String _ID = "_id";
        public static final String ID_GENRE = "idGenre";
        public static final String ID_MOVIE = "idMovie";

    }

}