package me.show.testscroll.dbPack;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import me.show.testscroll.R;
import me.show.testscroll.contentUtil.ControllerContent;

/**
 * Created by Anton Alexandrov on 31.07.15.
 */
public class DBHelper extends SQLiteOpenHelper {

    static final String DB_MOVIE = "create table " + Contract.Movie.TABLE_NAME + "("
            + Contract.Movie._ID + " integer primary key autoincrement, "
            + Contract.Movie.NAME + " text, "
            + Contract.Movie.PREVIEW + " text "
            +");";

    static final String DB_GENRE = "create table " + Contract.Genre.TABLE_NAME + "("
        + Contract.Genre._ID + " integer primary key autoincrement, "
        + Contract.Genre.NAME + " text, "
        + Contract.Genre.PREVIEW + " text "
        +");";

    static final String DB_GENRE_MOVIE = "create table " + Contract.GenreAndMovie.TABLE_NAME + "("
        + Contract.GenreAndMovie._ID + " integer primary key autoincrement, "
        + Contract.GenreAndMovie.ID_GENRE + " integer, "
        + Contract.GenreAndMovie.ID_MOVIE + " integer "
        +");";


    private Context context;
    private ControllerContent controllerContent = new ControllerContent();

    public DBHelper(Context context) {
        super(context, "testDB", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DB_MOVIE);
        db.execSQL(DB_GENRE);
        db.execSQL(DB_GENRE_MOVIE);

        controllerContent.addGenre(context, R.drawable.photo_3, "Ужасы");
        controllerContent.addGenre(context, R.drawable.photo_1, "Драмы");
        controllerContent.addGenre(context, R.drawable.photo_2, "Боевики");

        controllerContent.addMovie(context, R.drawable.big_photo_1, "Оригинальная подпись 1", new String[]{"Ужасы", "Драмы", "Боевики"});
        controllerContent.addMovie(context, R.drawable.big_photo_2, "Оригинальная подпись 2", new String[]{"Ужасы", "Драмы"});
        controllerContent.addMovie(context, R.drawable.big_photo_3, "Оригинальная подпись 3", new String[]{"Ужасы"});

        context = null;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


}
