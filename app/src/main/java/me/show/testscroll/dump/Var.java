package me.show.testscroll.dump;

import android.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple utility class which dumps the content of a object to {@code String}
 * <p/>
 * Date: Apr 5, 2010
 * Time: 1:19:53 PM
 *
 * @author Shanbo Li
 * @author Anton Alexandrov
 */

public class Var {
    static String trace(Object object) {
        if(object == null){
            return "null";
        }
        if (object instanceof int[]) {
            String str = "[";
            for(int i : (int[]) object){
                str += Integer.toString(i) + ", ";

            }
            str +="]";
            return str;
        }
        if (object instanceof String) {
            return (String) object;
        }
        if (object instanceof Integer) {
            return object.toString();
        }
        if(object instanceof Long){
            return object.toString();
        }
        if(object instanceof Float){
            return object.toString();
        }
        if(object instanceof Boolean){
            return object.toString();
        }
        if(object instanceof Calendar){
            Calendar timeZoom = (Calendar) object;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
            dateFormat.setTimeZone(timeZoom.getTimeZone());
            return dateFormat.format(timeZoom.getTime());
        }
        Field[] fields = object.getClass().getDeclaredFields();
        StringBuilder sb = new StringBuilder();
        sb.append(object.getClass().getSimpleName()).append('{');

        boolean firstRound = true;

        for (Field field : fields) {
            if (!firstRound) {
                sb.append(", ");
            }
            firstRound = false;
            field.setAccessible(true);
            try {
                final Object fieldObj = field.get(object);
                final String value;
                if (null == fieldObj) {
                    value = "null";
                } else {
                    value = fieldObj.toString();
                }
                sb.append('"' + field.getName() + '"').append('=').append('"').append(value).append('"');
            } catch (IllegalAccessException ignore) {
                //this should never happen
            }

        }

        sb.append('}');
        return sb.toString();
    }

    public static void dump(Object a) {
        Log.d("MyProject", Var.trace(a));
    }

    public static void dump(Object... arg) {
        String str = "";
        for (Object obj : arg) {
            str += " " + Var.trace(obj);
        }
        Log.d("MyProject", str);
    }

   public static Object get(Object obj, String key) {
       try {
           return obj.getClass().getField(key).get(obj);
       } catch (Exception e) {
           e.printStackTrace();
       }
       return null;
   }

    public static Object get(Class obj, String key) {
        try {
            return obj.getField(key).get(obj.newInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void set(Object obj, String key, Object value) {
        try {
            obj.getClass().getField(key).set(obj, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Object functionUse(Object obj, String functionName, Object params){
        try {
            Class c = obj.getClass();
            Class[] paramTypes = new Class[] {Object.class};
            Method method = null;
            method = c.getMethod(functionName, paramTypes);
            return method.invoke(obj, params);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

}