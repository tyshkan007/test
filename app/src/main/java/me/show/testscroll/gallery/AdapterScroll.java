package me.show.testscroll.gallery;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import me.show.testscroll.R;
import me.show.testscroll.dbPack.Contract;
import me.show.testscroll.dump.Var;
import me.show.testscroll.imageUtil.ImageUtil;

/**
 * Created by Anton Alexandrov on 03.09.15.
 * All rights reserved.
 */
public class AdapterScroll extends RecyclerView.Adapter<AdapterScroll.ViewHolder> {

    private Cursor cursor;
    private ImageUtil imageUtil;
    private Context context;

     public AdapterScroll(Context context){
         imageUtil = new ImageUtil();
         this.context = context;
     }

    public void setCursor(Cursor cursor) {
        this.cursor = cursor;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.slot_genre, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder view, int position) {
        int realPosition = position % cursor.getCount();
        if(cursor.moveToPosition(realPosition)) {
            int i = cursor.getColumnIndex(Contract.Genre.NAME);
            String str = cursor.getString(i);
            view.textVew.setText(str);
            String patch = cursor.getString(cursor.getColumnIndex(Contract.Genre.PREVIEW));
            view.imageView.setImageBitmap(imageUtil.getImageBitmap(context, patch));
        } else {
            cursor.close();
        }
    }

    @Override
    public int getItemCount() {
        if(cursor == null){
            return 0;
        }
        return Integer.MAX_VALUE;
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {

        public TextView textVew;
        public ImageView imageView;


        public ViewHolder(final View itemView) {
            super(itemView);
            textVew = (TextView) itemView.findViewById(R.id.textView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
        }



    }
}
