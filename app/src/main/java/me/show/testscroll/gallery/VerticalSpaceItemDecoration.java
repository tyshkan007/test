package me.show.testscroll.gallery;

import android.graphics.Rect;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;
import me.show.testscroll.dump.Var;

/**
 * Created by 13 on 05.09.2015.
 */
public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

    private final int mVerticalSpaceHeight;

    public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
        this.mVerticalSpaceHeight = mVerticalSpaceHeight;
    }

    int itemW = -1;
    int itemH;



    @Override
    public void getItemOffsets(final Rect outRect, final View view, final RecyclerView parent,
                               RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        if(itemW == -1) {
            itemW = 0;
            listener.view = view;
            listener.parent = parent;
            view.getViewTreeObserver().addOnGlobalLayoutListener(listener);
            return;
        }
        outRect.top = (parent.getHeight() - itemH) / 2;

    }

     Listener listener = new Listener();

     class Listener implements ViewTreeObserver.OnGlobalLayoutListener {

         View view;
         RecyclerView parent;


        @Override
        public void onGlobalLayout() {
            if (Build.VERSION.SDK_INT < 16) {
                view.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
            } else {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
            }

            itemH = view.getHeight();
            itemW = view.getWidth();
            Var.dump("sizeOne", itemH, itemW);
            parent.invalidateItemDecorations();
            view = null;
            parent = null;
        }
    }
}