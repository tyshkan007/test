package me.show.testscroll.gallery;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import me.show.testscroll.dump.Var;

/**
 * Created by 13 on 06.09.2015.
 */
public class NewRecyclerView extends RecyclerView {

    public NewRecyclerView(Context context) {
        super(context);
    }

    public NewRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NewRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    public boolean fling(int velocityX, int velocityY) {
        Var.dump(velocityX, velocityX);
        super.fling(velocityX, velocityY);
        return true;
    }


    private float xStart;
    private float yStart;

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        float x = e.getX();
        float y = e.getY();

        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                xStart = x;
                yStart = y;
            case MotionEvent.ACTION_UP: // отпускание
            case MotionEvent.ACTION_CANCEL:
                Var.dump("up", xStart - x);
                break;
        }
        return super.onTouchEvent(e);
    }
}
