package me.show.testscroll.gallery;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import me.show.testscroll.R;
import me.show.testscroll.dbPack.Contract;
import me.show.testscroll.dump.Var;

/**
 * Created by Anton Alexandrov on 02.09.15.
 * All rights reserved.
 */
public class Scroll extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>  {

    public static final int LOADER_GENRE = 0;
    public AdapterScroll adapter;
    private RecyclerView recyclerView;

    private final static int countItem = 7;
    private int overallXScroll = 0;


    private float x;
    private float y;
    private String sDown;
    private String sMove;
    private String sUp;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        RelativeLayout v = (RelativeLayout) inflater.inflate(R.layout.scroll_genre, null);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(100));
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
       super.onActivityCreated(savedInstanceState);
        adapter = new AdapterScroll(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);


        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(LOADER_GENRE, null, this);
        //loaderManager.initLoader(CAPITALS_LOADER_ID, null, this);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_GENRE:
                return new CursorLoader(getActivity(), Contract.Genre.CONTENT_URI, null, null, null, null);
            default:
                throw new IllegalArgumentException("Unknown loader id: " + id);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case LOADER_GENRE:
                adapter.setCursor(cursor);
                recyclerView.getLayoutManager().scrollToPosition(adapter.getItemCount()/2);
                return;
            default:
                throw new IllegalArgumentException("Unknown loader id: " + loader.getId());
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch(loader.getId()) {
            case LOADER_GENRE:
                adapter.setCursor(null);
            default:
                throw new IllegalArgumentException("Unknown loader id: " + loader.getId());
        }
    }

//    public void onStart() {
//        super.onStart();
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(errorReceiver,
//            new IntentFilter(Request.ERROR_LOAD));
//
//    }

//    public void onStop() {
//        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(errorReceiver);
//        super.onStop();
//    }


}
